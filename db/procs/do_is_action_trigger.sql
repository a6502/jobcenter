CREATE OR REPLACE FUNCTION jobcenter.do_is_action_trigger()
 RETURNS trigger
 LANGUAGE plpgsql
 SET search_path TO jobcenter, pg_catalog, pg_temp
AS $function$
BEGIN
	PERFORM
		 1
	FROM
		 actions
	WHERE
		action_id = NEW.action_id and type = 'action' ;
	IF NOT FOUND THEN
		-- emulate old error message
		RAISE EXCEPTION 'new row for relation "worker_actions" violates check constraint "check_is_action"'
			USING ERRCODE=23514;
	END IF;
	RETURN NEW;
END;
$function$
