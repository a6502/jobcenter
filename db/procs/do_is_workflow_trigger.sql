CREATE OR REPLACE FUNCTION jobcenter.do_is_workflow_trigger()
 RETURNS trigger
 LANGUAGE plpgsql
 SET search_path TO jobcenter, pg_catalog, pg_temp
AS $function$
BEGIN
	PERFORM
		1
	FROM
		actions
	WHERE
		action_id = NEW.workflow_id and type = 'workflow';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'new row violates check constraint "check_is_workflow"'
			USING ERRCODE=23514;
	END IF;
	RETURN NEW;
END;
$function$
