CREATE OR REPLACE FUNCTION jobcenter.do_check_job_is_waiting_trigger()
 RETURNS trigger
 LANGUAGE plpgsql
 SET search_path TO 'jobcenter', 'pg_catalog', 'pg_temp'
AS $function$
BEGIN
	--RAISE LOG 'do_check_job_is_waiting_trigger OLD % NEW %', OLD, NEW;
	PERFORM
		1
	FROM
		jobs
	WHERE
		job_id = NEW.job_id
		AND case when NEW.waiting THEN state='eventwait' ELSE state<>'eventwait' end;
	IF NOT FOUND THEN
		-- emulate old error message
		RAISE EXCEPTION 'new row for relation "event_subscriptions" violates check constraint "check_job_is_wating"'
			USING ERRCODE=23514;
	END IF;
	RETURN NEW;
END;
$function$
