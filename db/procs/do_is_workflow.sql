CREATE OR REPLACE FUNCTION jobcenter.do_is_workflow(integer)
 RETURNS boolean
 LANGUAGE sql
 SET search_path TO jobcenter, pg_catalog, pg_temp
AS $function$
-- not used anymore, needs to be deleted
-- once we add delete support to dbdings
select exists ( select 1 from actions where action_id = $1 and type = 'workflow' );
$function$
