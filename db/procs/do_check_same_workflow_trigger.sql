CREATE OR REPLACE FUNCTION jobcenter.do_check_same_workflow_trigger()
 RETURNS trigger
 LANGUAGE plpgsql
 SET search_path TO jobcenter, pg_catalog, pg_temp
AS $function$
BEGIN
	PERFORM 1 FROM
		tasks AS t1
		JOIN tasks AS t2
		ON (t1.workflow_id = t2.workflow_id)
	WHERE
		t1.task_id = NEW.from_task_id
		AND t2.task_id = NEW.to_task_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'new row for relation "next_tasks" violates check constraint "check_same_workflow"'
			USING ERRCODE=23514;
	END IF;
	RETURN NEW;
END;
$function$
