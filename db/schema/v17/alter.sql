
-- the schema gets updated before the stored procedures, but in this case
-- the schema depends on procedures being present so we create them here

CREATE OR REPLACE FUNCTION jobcenter.do_check_job_is_waiting_trigger()
 RETURNS trigger
 LANGUAGE plpgsql
 SET search_path TO 'jobcenter', 'pg_catalog', 'pg_temp'
AS $function$
BEGIN
	--RAISE LOG 'do_check_job_is_waiting_trigger OLD % NEW %', OLD, NEW;
	PERFORM
		1
	FROM
		jobs
	WHERE
		job_id = NEW.job_id
		AND case when NEW.waiting THEN state='eventwait' ELSE state<>'eventwait' end;
	IF NOT FOUND THEN
		-- emulate old error message
		RAISE EXCEPTION 'new row for relation "event_subscriptions" violates check constraint "check_job_is_wating"'
			USING ERRCODE=23514;
	END IF;
	RETURN NEW;
END;
$function$;

-- annoyinly enough we have to create the md5sums as well to make dbding update work
insert into _procs values ('do_check_job_is_waiting_trigger','d1397b6ba2a9bfcea4b1728c242f1109');

CREATE TRIGGER check_job_is_waiting_trigger AFTER INSERT OR UPDATE ON event_subscriptions FOR EACH ROW EXECUTE PROCEDURE do_check_job_is_waiting_trigger();

-- note the lovely typo
alter table event_subscriptions drop CONSTRAINT check_job_is_wating;

CREATE OR REPLACE FUNCTION jobcenter.do_is_action_trigger()
 RETURNS trigger
 LANGUAGE plpgsql
 SET search_path TO jobcenter, pg_catalog, pg_temp
AS $function$
BEGIN
	PERFORM
		 1
	FROM
		 actions
	WHERE
		action_id = NEW.action_id and type = 'action' ;
	IF NOT FOUND THEN
		-- emulate old error message
		RAISE EXCEPTION 'new row for relation "worker_actions" violates check constraint "check_is_action"'
			USING ERRCODE=23514;
	END IF;
	RETURN NEW;
END;
$function$;

insert into _procs values ('do_is_action_trigger','3f8f6df7239db258bf999ecd71ba882c');

CREATE TRIGGER check_is_action_trigger AFTER INSERT OR UPDATE ON worker_actions FOR EACH ROW EXECUTE PROCEDURE do_is_action_trigger();

alter table worker_actions drop constraint check_is_action;

CREATE OR REPLACE FUNCTION jobcenter.do_is_workflow_trigger()
 RETURNS trigger
 LANGUAGE plpgsql
 SET search_path TO jobcenter, pg_catalog, pg_temp
AS $function$
BEGIN
	PERFORM
		1
	FROM
		actions
	WHERE
		action_id = NEW.workflow_id and type = 'workflow';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'new row violates check constraint "check_is_workflow"'
			USING ERRCODE=23514;
	END IF;
	RETURN NEW;
END;
$function$;

insert into _procs values ('do_is_workflow_trigger','e3e76a92999375610797b7c40609234e');

CREATE TRIGGER check_is_workflow_trigger AFTER INSERT OR UPDATE ON tasks FOR EACH ROW EXECUTE PROCEDURE do_is_workflow_trigger();

alter table tasks drop constraint check_is_workflow;

CREATE TRIGGER check_is_workflow_trigger AFTER INSERT OR UPDATE OF workflow_id ON jobs FOR EACH ROW EXECUTE PROCEDURE do_is_workflow_trigger();

alter table jobs drop constraint check_is_workflow;

CREATE OR REPLACE FUNCTION jobcenter.do_check_same_workflow_trigger()
 RETURNS trigger
 LANGUAGE plpgsql
 SET search_path TO jobcenter, pg_catalog, pg_temp
AS $function$
BEGIN
	PERFORM 1 FROM
		tasks AS t1
		JOIN tasks AS t2
		ON (t1.workflow_id = t2.workflow_id)
	WHERE
		t1.task_id = NEW.from_task_id
		AND t2.task_id = NEW.to_task_id;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'new row for relation "next_tasks" violates check constraint "check_same_workflow"'
			USING ERRCODE=23514;
	END IF;
	RETURN NEW;
END;
$function$;

insert into _procs values ('do_check_same_workflow_trigger','562e280ef934112f7231494e812f8e7c');

CREATE TRIGGER check_same_workflow_trigger AFTER INSERT OR UPDATE ON next_tasks FOR EACH ROW EXECUTE PROCEDURE do_check_same_workflow_trigger();

alter table next_tasks drop constraint check_same_workflow;

alter table queued_events add unique(eventdata);

