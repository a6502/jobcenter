-- not worth it
-- create index "job_worker_id_idx" on jobs ( ((task_state->>'worker_id')::bigint) );

update action_inputs set "default"='null' where action_id=-9 and name='timeout';

insert into json_schemas (type,base,schema) values ('interval',false,'{"type": ["number", "string", "null"]}');

update action_inputs set type='interval' where action_id=-9 and name='timeout';

alter index jobs_timeout_idx rename to jobs_timeout_idx_old;

create index jobs_timeout_idx on jobs (timeout) where timeout is not null;

drop index jobs_timeout_idx_old;

-- alter index jobs_cookie_key rename to jobs_cookie_idx_old;

create unique index jobs_cookie_idx on jobs (cookie) where cookie is not null;

alter table jobs drop constraint jobs_cookie_key;

create index job_parentjob_id_idx on jobs (parentjob_id) where parentjob_id is not null;

drop index job_parentjob_id_index;

